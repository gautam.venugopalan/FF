# Feedforward filter calculations

Example notebook walking through how to calculate feedforward filters for the 40m. 

Recommended usage is to clone the `docker` image that has all the required python packages. For this, you need to 
1. [Install Docker](https://docs.docker.com/get-docker/) on your local machine
1. Login to docker using your albert.einstein credentials:
    ```sh 
    docker login containers.ligo.org
    ```
    and enter the credentials when prompted.
1. Pull the required docker image using
    ```sh
    docker pull -a containers.ligo.org/gautam.venugopalan/ffdocker
    ```

Then, clone this repo. After cloning, run (from **the cloned repo directory**):
```sh
docker run -it -p 8888:8888 -v $(pwd):/home/controls/localFiles containers.ligo.org/gautam.venugopalan/ffdocker:master
```
The argument `-v $(pwd):/home/controls/localFiles` attaches the current directory to `/home/controls/localFiles` on the docker container, so that you can run the jupyter notebook. Then, **from inside the container**, run

```sh
jupyter-lab --ip 0.0.0.0 --no-browser --allow-root
```

This will generate a few URL options, with which you can run the jupyter notebook in a browser on your **local machine**, i.e. not inside the container (just copy and paste the URL into a browser).
